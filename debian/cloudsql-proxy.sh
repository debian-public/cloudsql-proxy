#!/bin/bash

# A wrapper around /usr/bin/cloud_sql_proxy so we can pass options in a
# configuration file.

# Read the configuration file. This file should have the following
# format:
#
# dir=/run/cloudsql
# credential_file=/root/cloudsql.json
# instances=uit-acs-linux:us-west1:acs-linux-mysql-1=tcp:3306
#
# The "instances" option is MANDATORY. The others are OPTIONAL.
#
# These are passed as command line options to /usr/bin/cloud_sql_proxy

. /etc/cloudsql-proxy/options

if [[ -z "$dir" ]]; then
    dir="/run/cloudsql"
fi

if [[ -z "$credential_file" ]]; then
    echo "missing credentials file"
    exit 1
fi

if [[ -z "$instances" ]]; then
    echo "missing instances setting"
    exit 1
fi

/usr/bin/cloud_sql_proxy -dir=$dir -credential_file=$credential_file -instances=$instances
