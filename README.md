[[_TOC_]]

## Overview

This is the code to create a Debian package of Google's [Cloud SQL Proxy
program](https://cloud.google.com/sql/docs/mysql/sql-proxy). The package
contains the executable as well as a systemd service. There is a also a
wrapper script `cloudsql-proxy.sh` that is used by the service. This
wrapper script expects to find an options at
`/etc/cloudsql-proxy/options`. This options file contains lines that will
be passed to the `cloudsql-proxy` executable when the systemd service
starts.

Here is an example:
```
# /etc/cloudsql-proxy/options
dir=/run/cloudsql
credential_file=/etc/cloudsql-proxy/cloudsql.json
instances=uit-acs-linux:us-west1:acs-linux-mysql-1=tcp:3306
```
The above options file cause the `ckoudsql-proxy` service to run the following:
```
/usr/bin/cloudsql-proxy -dir=/run/cloudsql -credential_file=/etc/cloudsql-proxy/cloudsql.json -instances=uit-acs-linux:us-west1:acs-linux-mysql-1=tcp:3306
```

The `instances` line in `/etc/cloudsql-proxy/options` is _required_.

## How to build the Debian package

1. Download the latest cloud_sql_proxy executable (64-bit Linux):

        wget https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O cloud_sql_proxy

1. Clone this repository.

1. Change directory into the repository:

        cd cloudsql-proxy

1. Determine the version of the new release. For example, "1.16".

1. Prepare tar-ball

        ./prepare-tar-ball ../cloud_sql_proxy 1.16

1. Add a new changelog entry indicating the upstream version
terraform. For example, if the new version of the cloudsql-proxy binary is
"1.16", then run

        debchange -v 1.16-1 -D unstable
   Note that the "-1" indicates this is the first Debian packaging of version
   "1.16". Also, change "UNRELEASED" to "unstable".

1. Build the Debian package:

        pbuild sid

1. After a successful build, git commit the change to `changelog` and push.
